#!/usr/bin/env python3

import collections
import json

import emojiflags.lookup
import pycountry

from analyzer import OoniAnalyser


def main():
    analyzer = TorAnalyzer('Get countries where Tor might be blocked.')
    analyzer.analyze()


class TorAnalyzer(OoniAnalyser):
    country_data = collections.OrderedDict()

    def parse_report(self, data):
        if data['test_keys']['success'] is None:
            return
        cc = data['probe_cc']
        if cc not in self.country_data:
            self.country_data[cc] = self.get_new_country(cc)
        self.country_data[cc]['total_count'] += 1
        if data["test_keys"]["success"]:
            self.country_data[cc]['success_count'] += 1
        else:
            self.country_data[cc]['failure_count'] += 1

    def process_data(self):
        # calculate success percentage and identify blocking countries
        blocking_countries = []
        for country, data in sorted(self.country_data.items()):
            data['success_percent'] = round(data['success_count'] / data['total_count'] * 100, 2)
            if data['total_count'] > 5:
                if data['success_percent'] < self.args.threshold:
                    blocking_countries.append(country)
                print("%s: %d reports, %.2f success rate" % (
                    country, data['total_count'], data['success_percent']))

        # print blocking countries
        blocking_countries = sorted(blocking_countries)
        print()
        print("Blocking Countries: %s" % str(blocking_countries))
        print()
        for country in blocking_countries:
            print("%s: %s" % (country, str(self.country_data[country])))

        # write a JSON file with all data to be published on the web
        if self.args.json:
            with open('countries.json', 'w') as f:
                f.write(json.dumps(list(self.country_data.values()), indent=4))

    @staticmethod
    def get_new_country(cc):
        try:
            country = pycountry.countries.get(alpha_2=cc).name
            flag = emojiflags.lookup.lookup(cc)
            country = "%s %s" % (flag, country)
        except KeyError:
            country = cc
        return {
            'country': country,
            'total_count': 0,
            'success_count': 0,
            'failure_count': 0,
            'success_percent': None,
        }


if __name__ == "__main__":
    main()
