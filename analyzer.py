import argparse
import json
import lzma
import os
import re
import sys
from datetime import datetime

from tqdm import tqdm


class Analyser:

    def __init__(self, description):
        self.args = None
        self.description = description
        parser = self.init_arg_parser()
        self.args = parser.parse_args()

    def init_arg_parser(self):
        parser = argparse.ArgumentParser(description=self.description)
        parser.add_argument('--since', dest='since', type=self.date, metavar='YYYY-MM-DD',
                            help='only consider reports after that date, e.g. 2017-12-24')
        parser.add_argument('-j', '--json', dest='json', action='store_true',
                            help='create a JSON file with the results')
        return parser

    def analyze(self):
        raise NotImplementedError()

    @staticmethod
    def date(date_str):
        # Converts a string supplied at the command line into a date
        return datetime.strptime(date_str, "%Y-%m-%d")

    @staticmethod
    def fail(msg=""):
        sys.stderr.write("Error: %s\n" % msg)
        sys.exit(1)


class OoniAnalyser(Analyser):
    FILE_REGEX = re.compile("^(\d{8}T\d{6}Z)-([A-Z]{2})-")
    start_date = None

    def init_arg_parser(self):
        parser = super().init_arg_parser()
        parser.add_argument('path', metavar='path', type=str,
                            help='directory with OONI reports as supplied by ooni-sync')
        parser.add_argument('-t', '--success-threshold', dest='threshold', type=float, default=50,
                            metavar='PERCENT',
                            help='consider countries as blocking with a success rate less than this'
                                 + '. default: 50')
        return parser

    def use_country(self, cc):
        return True

    def use_file(self, file):
        if not file.endswith('.json.xz'):
            return False
        match = self.FILE_REGEX.match(file)
        if not match:
            self.fail("File in unexpected format: %s" % file)
        # get report date from file name
        file_date = datetime.strptime(match.group(1), '%Y%m%dT%H%M%SZ')
        # only consider reports since the supplied date
        if self.args.since and file_date < self.args.since:
            return False
        # only consider country if told to
        if not self.use_country(match.group(2)):
            return False
        # remember file date of the first file that we will use
        if self.start_date is None:
            self.start_date = file_date
        return True

    def analyze(self):
        if not os.path.isdir(self.args.path):
            self.fail("Could not find directory '%s'" % self.args.path)

        # get success and failure counts per country from report data
        for file in tqdm(sorted(os.listdir(self.args.path)), desc="Parsing Reports"):
            # check if we should be using this file
            if not self.use_file(file):
                continue
            # open compressed report file
            with lzma.open(os.path.join(self.args.path, file), 'r') as f:
                for line in f.readlines():
                    data = json.loads(line)
                    self.parse_report(data)

        self.process_data()

    def parse_report(self, data):
        raise NotImplementedError()

    def process_data(self):
        raise NotImplementedError()
