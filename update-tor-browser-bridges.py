#!/usr/bin/env python3

import re

import requests

URL = 'https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/PTConfigs/bridge_prefs.js'

REGEX = re.compile(
    r'pref\("extensions\.torlauncher\.default_bridge\.\w+\.\d+", ?"(\w+ [0-9.]+:\d+) ')


def main():
    bridges = []
    response = requests.get(URL)
    for line in response.text.splitlines():
        if not line.startswith('pref("extensions.torlauncher.default_bridge.'):
            continue
        match = REGEX.match(line)
        if match:
            bridges.append(match.group(1))

    with open('tor-browser-bridges', 'w') as f:
        for bridge in bridges:
            f.write("%s\n" % bridge)


if __name__ == "__main__":
    main()
