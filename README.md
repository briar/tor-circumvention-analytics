This is a collection of scripts
that analyze [OONI](https://ooni.io/) data
in order to find out
in which countries [Tor](https://www.torproject.org/) is blocked.

The official repository is available at:
https://code.briarproject.org/tor/tor-circumvention-analytics

# How to use

## Downloading OONI reports

First of all, we need to download OONI reports for this script to analyze.
We will be using [ooni-sync](https://www.bamsoftware.com/software/ooni-sync/)
([Github Mirror](https://github.com/TheTorProject/ooni-sync)) for this.

Please install ooni-sync like described on their homepage
and then download reports like this:

    ooni-sync -xz -directory data/vanilla_tor/ test_name=vanilla_tor since=2018-01-01

This will download all reports created since the beginning of 2018
for the `vanilla_tor` test into a folder called `data/vanilla_tor`
and compresses them to save disk space.

## Install Python Dependencies

    pip3 install -r requirements.txt

## Find countries that might block Tor

Make sure that all reports for the time frame you are interested in
have been downloaded as described in the previous section.

Then run the following command to get a list of countries
that might be blocking Tor.

    countries-that-block-tor.py ooni-sync/data/vanilla_tor/
    
You can also restrict the time-frame of the analysis with the `--since` parameter.
When older reports are available, these will be ignored.

    countries-that-block-tor.py --since=2017-12-24 ooni-sync/data/vanilla_tor/

## Find countries that might block Tor bridges

Download OONI reports for `tcp_connect` instead of `vanilla_tor`.
Use ooni-sync as explained above.

Then run the following command to get a CSV file that helps with the analysis.

    countries-that-block-bridges.py ooni-sync/data/tcp_connect/

# Limitations

Please note that the output of the scripts might give false negatives,
i.e. other countries not listed might also be blocking Tor.
This can be due to limited OONI data, wrong GeoIP classifications,
flaky national firewalls, sophisticated blocking techniques or other reasons.

# Publishing results on the web

Expose the `www` folder with a webserver.
Copy or symlink these generated files into the folder:
* `countries.json`
* `countries-bridges.json`
* `countries-bridge-ratio.json`

## Periodically update data

To keep the published data up do date,
you might want to set up a cronjob that runs the following scripts:

* `ooni-sync` for `vanilla_tor`
* `ooni-sync` for `tcp_connect`
* `update-tor-browser-bridges.py` to update `tor-browser-bridges`
* `countries-that-block-tor.py --json` to produce `countries.json`
* `countries-that-block-bridges.py --json` to produce `countries-bridges.json`
* `tor-metrics-bridges.py --json` to produce `countries-bridge-ratio.json`
