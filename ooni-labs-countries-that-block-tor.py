#!/usr/bin/env python3

import csv
import urllib.request
import codecs


url = 'http://labs.ooni.io/data/vanilla-tor/20171130-vanilla_tor-stats.csv'
stream = urllib.request.urlopen(url)
data = csv.DictReader(codecs.iterdecode(stream, 'utf-8'))

blocked_countries = []
for row in data:
    if int(row['total_count']) < 5:
        continue
    if float(row['success_perc']) > 0.5:
        continue
    blocked_countries.append(row['probe_cc'])

print(blocked_countries)
