#!/usr/bin/env python3
#
# This file uses code in the public domain from:
# https://people.torproject.org/~dcf/graphs/ooni_tor_bridge_reachability/2017-01-11/
#

import calendar
import csv
import time

from ooni_analyzer import OoniAnalyser

COUNTRIES = [
    'BY', 'CN', 'EG', 'TR', 'IR',  # Tor assumed to be blocked
    'US', 'GB', 'DE', 'IT',  # Tor should not be blocked (control group)
]


def main():
    analyzer = BridgeTimeAnalyzer('Create CSV for countries where Tor bridges might be blocked.')
    analyzer.analyze()


class BridgeTimeAnalyzer(OoniAnalyser):
    field_names = (
        "date",
        "site",
        "host",
        "port",
        "elapsed",
        "success",
        "errno",
        "errmsg",
    )
    bridge_data = {}

    @staticmethod
    def parse_timestamp_utc(ts):
        return calendar.timegm(time.strptime(ts, "%Y-%m-%d %H:%M:%S"))

    @staticmethod
    def parse_input(s):
        parts = s.split(" ", 1)
        if len(parts) == 1:
            protocol = None
            host_port = s
        elif len(parts) == 2:
            protocol = parts[0]
            host_port = parts[1]
        else:
            raise ValueError(s)

        parts = host_port.rsplit(":", 1)
        if len(parts) == 2:
            host = parts[0]
            port = parts[1]
        else:
            raise ValueError(s)

        return protocol, host, port

    def parse_report(self, data):
        # filter out measurements with missing information
        test_name = data.get("test_name")
        if test_name != "tcp_connect":
            return
        if 'test_class' not in data['annotations']:
            return
        if data['annotations']['test_class'] != 'tor_bridge_reachability':
            return
        if 'connection' not in data['test_keys']:
            return
        if data["probe_cc"] not in COUNTRIES:
            return

        try:
            protocol, host, port = self.parse_input(data["input"])
        except ValueError:
            raise RuntimeError("cannot parse input: %r" % data["input"])

        row = {
            "date": self.parse_timestamp_utc(data["measurement_start_time"]),
            "site": data["probe_cc"].upper(),
            "host": host,
            "port": port,
            "elapsed": data["test_runtime"],
        }

        connection = data["test_keys"]["connection"]
        if connection == "success":
            row.update({
                "success": "True",
                "errno": "",
                "errmsg": "",
            })
        elif connection == "connection_refused_error":
            row.update({
                "success": "False",
                "errno": "111",
                "errmsg": "[Errno 111] Connection refused",
            })
        elif connection in ("generic_timeout_error", "tcp_timed_out_error"):
            row.update({
                "success": "False",
                "errno": "",
                "errmsg": "timed out",
            })
        elif connection == "connect_error":
            # https://twistedmatrix.com/documents/current/api/twisted.internet.error.ConnectError.html
            # "An error occurred while connecting", not otherwise specified.
            row.update({
                "success": "False",
                "errno": "",
                "errmsg": "ooni-connect_error",
            })
        elif connection == "dns_lookup_error":
            # This is a bug in a couple of early reports. Ignore it.
            return
        else:
            raise RuntimeError("unknown connection status %r" % connection)

        key = data["input"]
        if key not in self.bridge_data:
            self.bridge_data[key] = []
        self.bridge_data[key].append(row)

    def process_data(self):
        # write CSV file
        with open('bridge-timeline.csv', 'w', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=self.field_names)
            for data in self.bridge_data.values():
                if len(data) < 500:
                    continue
                for row in data:
                    writer.writerow(row)


if __name__ == "__main__":
    main()
