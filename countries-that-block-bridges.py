#!/usr/bin/env python3

import csv
import datetime
import json
import statistics

import pendulum
from tqdm import tqdm

from analyzer import OoniAnalyser

BLOCKING_COUNTRIES = ['BY', 'CN', 'EG', 'TR', 'IR']
CONTROL_COUNTRIES = ['US', 'GB', 'DE', 'IT']

UNIT = 'days'  # hours, days, weeks
INTERVAL = 1
BRIDGE_CONTROL_THRESHOLD = 80  # only consider bridges with a control success rate higher than this

FAILURES = [
    'generic_timeout_error',
    'connection_refused_error',
    'connect_error',
    'tcp_timed_out_error',
]


def main():
    analyzer = BridgeAnalyzer('Get countries where Tor bridges might be blocked.')
    analyzer.analyze()


class BridgeAnalyzer(OoniAnalyser):
    pre_report_init = False
    period = None
    blocking_countries = []
    control_countries = []
    countries = []
    bridge_data = {}

    def init_arg_parser(self):
        parser = super().init_arg_parser()
        parser.add_argument('-n', '--nicknames', dest='nicknames', action='store_true',
                            help='replace bridge addresses with nicknames')
        parser.add_argument('--control-threshold', type=float, dest='control_threshold',
                            default=BRIDGE_CONTROL_THRESHOLD, metavar='PERCENT',
                            help='ignore bridges with a control success rate lower than this'
                                 + '. default: %d' % BRIDGE_CONTROL_THRESHOLD)
        parser.add_argument('--bucket-size', type=int, metavar='NUM', dest='bucket_size',
                            default=INTERVAL,
                            help='size of the buckets reports are aggregated in. default: %d'
                                 % INTERVAL)
        parser.add_argument('--bucket-unit', choices=['hours', 'days', 'weeks'], dest='bucket_unit',
                            default=UNIT,
                            help='unit of the buckets reports are aggregated in. default: %s'
                                 % UNIT)
        parser.add_argument('-c', '--countries', nargs='+', metavar='COUNTRY_CODE',
                            default=BLOCKING_COUNTRIES,
                            help='list of country codes to consider for blocking')
        parser.add_argument('--control-countries', nargs='+', dest='control_countries',
                            metavar='COUNTRY_CODE', default=CONTROL_COUNTRIES,
                            help='list of country codes of countries in the control group, '
                            + 'i.e. countries unlikely to block Tor.' )
        return parser

    def analyze(self):
        self.blocking_countries = [country.upper() for country in self.args.countries]
        self.control_countries = [country.upper() for country in self.args.control_countries]
        self.countries = self.blocking_countries + self.control_countries
        super().analyze()

    def use_country(self, cc):
        return cc in self.countries

    @staticmethod
    def use_report(data):
        # filter out measurements with missing information
        if 'test_class' not in data['annotations']:
            return False
        if data['annotations']['test_class'] != 'tor_bridge_reachability':
            return False
        if 'connection' not in data['test_keys']:
            return False
        return True

    def pre_parse_report(self):
        # convert date of first report to clean pendulum date
        timestamp = self.start_date.replace(tzinfo=datetime.timezone.utc).timestamp()
        time_unit = self.args.bucket_unit.rstrip('s')
        self.start_date = pendulum.from_timestamp(timestamp).start_of(time_unit)
        self.period = pendulum.period(self.start_date, pendulum.now())
        # remember to not call this again
        self.pre_report_init = True

    def parse_report(self, data):
        if not self.use_report(data):
            return
        # perform one-time initialization after we know the self.start_date
        if not self.pre_report_init:
            self.pre_parse_report()
        # initialize bridge data if needed
        bridge = data['input']
        if bridge not in self.bridge_data:
            self.bridge_data[bridge] = self.get_new_bridge()
        self.bridge_data[bridge]['total'] += 1
        # retrieve current bucket
        dt = pendulum.from_format(data['test_start_time'], 'YYYY-MM-DD HH:mm:ss')
        bucket = self.get_bucket(bridge, dt)
        # add results per country
        cc = data['probe_cc']
        if cc not in bucket['countries']:
            bucket['countries'][cc] = self.get_new_country()
        # add results and counts to bucket
        result = data['test_keys']['connection']
        if result == 'success':
            bucket['success'] = bucket['success'] or True
            bucket['countries'][cc]['success'] += 1
        elif result in FAILURES:
            bucket['countries'][cc]['failure'] += 1
        elif result == 'dns_lookup_error':
            return  # This is a bug in a couple of early reports. Ignore it.
        else:
            self.fail("Unknown connection result: %s" % result)

    def count_totals_for_successful_buckets(self):
        for bridge, data in tqdm(self.bridge_data.items(), desc="Counting Totals"):
            for bucket in data['buckets']:
                # don't count bucket if no one could connect to bridge in it
                if not bucket['success']:
                    continue
                # go through all countries
                for cc, country in bucket['countries'].items():
                    if cc not in data['countries']:
                        data['countries'][cc] = self.get_new_country()
                    data['countries'][cc]['success'] += country['success']
                    data['countries'][cc]['failure'] += country['failure']

    def get_field_names(self):
        blocking_total = [cc + "#" for cc in self.blocking_countries]
        control_total = [cc + "#" for cc in self.control_countries]
        return ('bridge', 'type', 'total') + \
            tuple(sorted(self.blocking_countries)) + \
            tuple(['ctrl'] + sorted(self.control_countries)) + \
            tuple(sorted(blocking_total)) + \
            tuple(sorted(control_total))

    def process_data(self):
        self.count_totals_for_successful_buckets()
        nicknames, types = self.get_nicknames_and_types()
        # re-order data for JSON export
        output_dict = {}
        success_rates = {}
        for bridge, data in tqdm(self.bridge_data.items(), desc="Calculating Success Rate"):
            # add bridge name
            if bridge not in output_dict:
                name = nicknames.get(bridge, bridge)
                output_dict[bridge] = {'bridge': name}
                output_dict[bridge]["type"] = types.get(bridge, None)
            # get total and ignore two few measurements
            if data['total'] is None or data['total'] < 500:
                del output_dict[bridge]
                continue
            output_dict[bridge]['total'] = data['total']
            # go through all buckets
            for cc, country in data['countries'].items():
                # calculate success rate
                total = country['success'] + country['failure']
                success_rate = round(country['success'] / total * 100, 2)
                output_dict[bridge][cc] = success_rate
                # add total count
                output_dict[bridge][cc + "#"] = total
            # calculate median success rate of control group
            rates = []
            for cc in self.control_countries:
                if cc in output_dict[bridge]:
                    rates.append(output_dict[bridge][cc])
            if len(rates) > 0:
                output_dict[bridge]['ctrl'] = round(statistics.median(rates), 2)
            else:
                del output_dict[bridge]
                continue
            # remember success rates for blocking countries
            if output_dict[bridge]['ctrl'] >= self.args.control_threshold:
                for cc in self.blocking_countries:
                    bridge_type = output_dict[bridge]["type"]
                    if bridge_type == "B" or bridge_type == "TB":
                        if cc not in output_dict[bridge]:
                            continue
                        success_rate = output_dict[bridge][cc]
                        if cc not in success_rates:
                            success_rates[cc] = []
                        success_rates[cc].append(success_rate)

        blocking_countries = []
        for cc, rates in success_rates.items():
            median = round(statistics.median(rates), 2)
            print("%s: %f" % (cc, median))
            if median < self.args.threshold:
                blocking_countries.append(cc)

        # print blocking countries
        blocking_countries = sorted(blocking_countries)
        print()
        print("Blocking Countries: %s" % str(blocking_countries))
        print()

        # write JSON file
        if self.args.json:
            with open('countries-bridges.json', 'w') as f:
                json.dump(list(output_dict.values()), f)

    def get_new_bridge(self):
        bridge = {
            'total': 0,
            'buckets': [],
            'countries': {},
        }
        # initialize time buckets
        for dt in self.period.range(self.args.bucket_unit, amount=self.args.bucket_size):
            bucket = {
                'dt': str(dt),
                'success': False,
                'countries': {},
            }
            bridge['buckets'].append(bucket)
        return bridge

    @staticmethod
    def get_new_country():
        return {
            'success': 0,
            'failure': 0,
        }

    def get_bucket(self, bridge, dt):
        duration = dt - self.start_date
        if self.args.bucket_unit == 'weeks':
            diff = duration.total_weeks()
        elif self.args.bucket_unit == 'days':
            diff = duration.total_days()
        elif self.args.bucket_unit == 'hours':
            diff = duration.total_hours()
        else:
            raise RuntimeError("Unknown time unit: %s" % self.args.bucket_unit)
        pos = int(diff / self.args.bucket_size)
        return self.bridge_data[bridge]['buckets'][pos]

    def get_nicknames_and_types(self):
        """
        Returns a directory with bridge OONI names to nicknames.

        tor-bridges-ip-port.csv was retrieved and can be updated from
        https://github.com/OpenObservatory/ooni-resources/blob/master/bridge_reachability/tor-bridges-ip-port.csv
        """
        nicknames = {}
        types = {}
        tor_bridges = BridgeAnalyzer.get_tor_browser_bridges()
        with open('tor-bridges-ip-port.csv', 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                key = "%s:%s" % (row['host'], row['port'])
                if self.args.nicknames:
                    value = "%s:%s" % (row['nickname'], row['port'])
                else:
                    value = key
                typ = "DA"  # OONI tests include directory authorities (and their fallbacks)
                if row['protocol'] != 'or_port' and row['protocol'] != 'dir_port' and \
                        row['protocol'] != 'ssh':
                    key = "%s %s" % (row['protocol'], key)
                    value = "%s %s" % (row['protocol'], value)
                    typ = "B"
                if row['protocol'] == 'ssh':
                    typ = "B"
                if key in tor_bridges:
                    typ = "TB"
                nicknames[key] = value
                types[key] = typ
        return nicknames, types

    @staticmethod
    def get_tor_browser_bridges():
        bridges = []
        with open('tor-browser-bridges', 'r') as f:
            for l in f.readlines():
                bridges.append(l.rstrip('\n'))
        return bridges


if __name__ == "__main__":
    main()
