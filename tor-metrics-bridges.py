#!/usr/bin/env python3

import codecs
import csv
import statistics
import urllib.request
from datetime import datetime, timedelta

import emojiflags.lookup
import pycountry
import json

from analyzer import Analyser

DEFAULT_DAYS = 90
MIN_USERS = 10
BASE_URL_RELAY = 'https://metrics.torproject.org/userstats-relay-country.csv'
BASE_URL_BRIDGE = 'https://metrics.torproject.org/userstats-bridge-country.csv'


def main():
    analyzer = TorMetricsAnalyzer('Get countries where Tor bridges might be blocked.')
    analyzer.analyze()


class TorMetricsAnalyzer(Analyser):

    def __init__(self, description):
        super().__init__(description)
        if self.args.since:
            self.start = self.args.since.strftime("%Y-%m-%d")
        else:
            self.start = (datetime.now() - timedelta(days=DEFAULT_DAYS)).strftime("%Y-%m-%d")
        self.end = datetime.now().strftime("%Y-%m-%d")

    def init_arg_parser(self):
        parser = super().init_arg_parser()
        parser.add_argument('-t', '--bridge-threshold', dest='threshold', type=float, default=0.5,
                            metavar='RATIO',
                            help='consider countries as blocking with a bridge ratio more than this'
                                 + '. default: 0.5')
        return parser

    def analyze(self):
        data = []
        for country in pycountry.countries:
            cc = country.alpha_2
            users_relay = self.get_median_users(BASE_URL_RELAY, cc)
            users_bridge = self.get_median_users(BASE_URL_BRIDGE, cc)
            total = users_relay + users_bridge
            if total < MIN_USERS:
                continue
            bridge_ratio = round(users_bridge / total, 2)
            print("%s: %d relay users, %d bridge users,  %.2f bridge ratio" %
                  (cc, users_relay, users_bridge, bridge_ratio))
            data.append({
                'country': "%s %s" % (emojiflags.lookup.lookup(cc), country.name),
                'total': total,
                'relay_users': users_relay,
                'bridge_users': users_bridge,
                'bridge_ratio': bridge_ratio,
            })
        # write a JSON file with all data to be published on the web
        if self.args.json:
            with open('countries-bridge-ratio.json', 'w') as f:
                f.write(json.dumps(data))

    def get_median_users(self, base_url, cc):
        url = base_url + self.get_query_string(cc)
        stream = urllib.request.urlopen(url)
        data = csv.DictReader(
            (row for row in codecs.iterdecode(stream, 'utf-8') if not row.startswith('#'))
        )
        users = []
        for row in data:
            users.append(int(row['users']))
        if len(users) == 0:
            return 0
        return statistics.median(users)

    def get_query_string(self, cc):
        return "?start=%s&end=%s&country=%s" % (self.start, self.end, cc.lower())


if __name__ == "__main__":
    main()
