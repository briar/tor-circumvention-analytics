$("#vanilla_tor").tabulator({
  layout:"fitData",
  columns:[
    {title:"Country", field:"country"},
    {title:"# Measurements", field:"total_count", align:"center"},
    {title:"Success Rate", field:"success_percent", align:"center", sorter:"number", formatter:function(cell, formatterParams){
      var value = cell.getValue();
      if(value < 50) {
        return "<span style='color:red; font-weight:bold;'>" + value + "</span>";
      } else{
        return value;
      }
    }},
    {title:"# Successes", field:"success_count", align:"center"},
    {title:"# Failures", field:"failure_count", align:"center"},
  ],
  initialSort:[
          {column:"success_percent", dir:"asc"},
      ]
});

$.getJSON("countries.json")
.done(function( json, textStatus, jqXHR ) {
  $("#last-modified").html(jqXHR.getResponseHeader("Last-Modified"));
  $("#vanilla_tor").tabulator("setData", json);
  showOoniBlocking();
})

$("#ooni-filter").click(showOoniBlocking);
$("#ooni-filter-clear").click(showOoniAll);

function showOoniBlocking() {
  $("#vanilla_tor").tabulator("setFilter", "success_percent", "<", 50);
  $("#ooni-filter").hide();
  $("#ooni-filter-clear").show();
}

function showOoniAll() {
  $("#vanilla_tor").tabulator("clearFilter");
  $("#ooni-filter").show();
  $("#ooni-filter-clear").hide();
}

/* Tor Metrics */

$("#tor-metrics").tabulator({
  layout:"fitData",
  columns:[
    {title:"Country", field:"country"},
    {title:"Total Users", field:"total", align:"center"},
    {title:"Relay Users", field:"relay_users", align:"center"},
    {title:"Bridge Users", field:"bridge_users", align:"center"},
    {title:"Bridge Ratio", field:"bridge_ratio", align:"center", sorter:"number", formatter:function(cell, formatterParams){
      var value = cell.getValue();
      if(value >= 0.5) {
        return "<span style='color:red; font-weight:bold;'>" + value + "</span>";
      } else{
        return value;
      }
    }},
  ],
  initialSort:[
          {column:"bridge_ratio", dir:"desc"},
      ]
});

$.getJSON("countries-bridge-ratio.json")
.done(function( json ) {
  $("#tor-metrics").tabulator("setData", json);
  showMetricsBlocking();
})

$("#metrics-filter").click(showMetricsBlocking);
$("#metrics-filter-clear").click(showMetricsAll);

function showMetricsBlocking() {
  $("#tor-metrics").tabulator("setFilter", "bridge_ratio", ">=", 0.5);
  $("#metrics-filter").hide();
  $("#metrics-filter-clear").show();
}

function showMetricsAll() {
  $("#tor-metrics").tabulator("clearFilter");
  $("#metrics-filter").show();
  $("#metrics-filter-clear").hide();
}