$("#bridge_reachability").tabulator({
  layout:"fitData",
  // TODO get this from JSON instead of hardcoding it
  columns:[
    {title:"Node", field:"bridge"},
    {title:"Type", field:"type"},
    {title:"Total", field:"total"},
    {
        title: "Blocking Countries",
        columns:[
            {title:"🇧🇾 BY", field:"BY", formatter:formatColor},
            {title:"🇨🇳 CN", field:"CN", formatter:formatColor},
            {title:"🇪🇬 EG", field:"EG", formatter:formatColor},
            {title:"🇮🇷 IR", field:"IR", formatter:formatColor},
            {title:"🇹🇷 TR", field:"TR", formatter:formatColor},
        ],
    },
    {
        title: "Control Group",
        columns:[
            {title:"Median", field:"ctrl", sorter:"number"},
            {title:"🇩🇪 DE", field:"DE", formatter:formatColor},
            {title:"🇬🇧 GB", field:"GB", formatter:formatColor},
            {title:"🇮🇹 IT", field:"IT", formatter:formatColor},
            {title:"🇺🇸 US", field:"US", formatter:formatColor},
        ],
    },
    {
        title: "# Reports Blocking Countries",
        columns:[
            {title:"🇧🇾 BY", field:"BY#"},
            {title:"🇨🇳 CN", field:"CN#"},
            {title:"🇪🇬 EG", field:"EG#"},
            {title:"🇮🇷 IR", field:"IR#"},
            {title:"🇹🇷 TR", field:"TR#"},
        ],
    },
    {
        title: "# Reports Control Group",
        columns:[
            {title:"🇩🇪 DE", field:"DE#"},
            {title:"🇬🇧 GB", field:"GB#"},
            {title:"🇮🇹 IT", field:"IT#"},
            {title:"🇺🇸 US", field:"US#"},
        ],
    },
  ],
  initialSort:[
          {column:"ctrl", dir:"desc"},
      ]
});

var bridgeFilter = {field:"type", type:"in", value:["TB", "B"]}
var ctrlFilter = {field:"ctrl", type:">=", value:80}

$.getJSON("countries-bridges.json")
.done(function( json, textStatus, jqXHR ) {
  $("#last-modified").html(jqXHR.getResponseHeader("Last-Modified"));
  $("#bridge_reachability").tabulator("setData", json);
  applyFilter();
})

$("#showB").change(function() {
  showType("B", $(this).is(':checked'));
});
$("#showTB").change(function() {
  showType("TB", $(this).is(':checked'));
});
$("#showDA").change(function() {
  showType("DA", $(this).is(':checked'));
});

function showType(type, show) {
    if (show) bridgeFilter.value.push(type);
    else {
        var index = bridgeFilter.value.indexOf(type);
        if (index > -1) {
          bridgeFilter.value.splice(index, 1);
        }
    }
    applyFilter();
}

$('#slider').on("change mousemove", function() {
    console.log($(this).val());
    $('#slider-value').html($(this).val());
    ctrlFilter.value = $(this).val()
    applyFilter();
});

function applyFilter() {
    $("#bridge_reachability").tabulator("setFilter", [
        bridgeFilter,
        ctrlFilter,
    ]);
}

function formatColor(cell, formatterParams){
    var value = cell.getValue();
    cell.getElement().css("background-color", getColor(value));
    return value;
}

function getColor(value){
    //value from 0 to 1
    var hue=((value/100)*80).toString(10);
    return ["hsl(",hue,",80%,50%)"].join("");
}